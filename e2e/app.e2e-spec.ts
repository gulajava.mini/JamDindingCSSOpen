import { JamQuotesPage } from './app.po';

describe('jam-quotes App', () => {
  let page: JamQuotesPage;

  beforeEach(() => {
    page = new JamQuotesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
