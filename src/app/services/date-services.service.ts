import {Injectable} from "@angular/core";
import * as moment from "moment";
import {Observable} from "rxjs";


@Injectable()
export class DateServicesService {

  constructor() {

  }

  getTanggalObsevable(): any {

    moment.locale('id');

    let observableDate = Observable.create(
      observer => {

        let momentlocale = moment().locale('id');
        let susunanWaktuTanggal: string = momentlocale.format("DD MMMM YYYY");

        observer.next(susunanWaktuTanggal);
        observer.complete();
      }
    )
      .catch(
        (error) => (Observable.throw(error))
      );

    return observableDate;
  }


}
